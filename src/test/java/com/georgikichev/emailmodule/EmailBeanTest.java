/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.georgikichev.emailmodule;

import java.util.List;
import jodd.mail.ReceivedEmail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author GoGo
 */
public class EmailBeanTest {
    
    public EmailBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of send method, of class EmailBean.
     */
    @Test
    public void testSend() {
        
        String subj = "test normal send";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
        /**
     * Test of constructor without subject, of class EmailBean.
     */
    @Test
    public void testCreateNoSubject() {
        
        String subj = "";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        try{
            EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);      
        }
        catch(IllegalArgumentException e)
        {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }
        /**
     * Test of constructor without to, of class EmailBean.
     */
    @Test
    public void testCreateNoTo() {
        
        String subj = "test";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        try{
            EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);      
        }
        catch(IllegalArgumentException e)
        {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }    
    
        /**
     * Test of constructor without from, of class EmailBean.
     */
    @Test
    public void testCreateNoFrom() {
        
        String subj = "test";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        try{
            EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);      
        }
        catch(IllegalArgumentException e)
        {
            assertTrue(e instanceof IllegalArgumentException);
        }
    }      
     /*
     * Test of send method with cc, of class EmailBean.
     */
    @Test
    public void testSendCC() {
        
        String subj = "test CC";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [2];
        String [] bcc = new String [1];
        cc[0] = "georgiemail03@gmail.com";
        cc[1] = "georgiemail004@gmail.com";
        bcc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
    /**
     * Test of send method with bcc, of class EmailBean.
     */
    @Test
    public void testSendBCC() {
        
        String subj = "test BCC";
        String message = "Please work";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [2];
        bcc[0] = "georgiemail03@gmail.com";
        bcc[1] = "georgiemail004@gmail.com";
        cc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
        /**
     * Test of send method with bcc with attachments, of class EmailBean.
     */
    @Test
    public void testSendBCCAttch() {
        
        String subj = "test BCC";
        String message = "Please work";
        String attach = "jpg1.jpg";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [2];
        bcc[0] = "georgiemail03@gmail.com";
        bcc[1] = "georgiemail004@gmail.com";
        cc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
        /**
     * Test of send method with cc with attchments, of class EmailBean.
     */
    @Test
    public void testSendCCAttach() {
        
        String subj = "test BCC";
        String message = "Please work";
        String attach = "jpg1.jpg";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [2];
        bcc[0] = "georgiemail03@gmail.com";
        bcc[1] = "georgiemail004@gmail.com";
        cc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
    /**
     * Test of send method with HTML, of class EmailBean.
     */
    @Test
    public void testSendHTML() {
        
        String subj = "test HTML";
        String message = "Please work html";
        String attach = "";
        String html = "<h1> test html <h1>";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
           /**
     * Test of send method with cc with html, of class EmailBean.
     */
    @Test
    public void testSendCCHTML() {
        
        String subj = "test BCC";
        String message = "Please work";
        String attach = "";
        String html = "<a1> html test <a1>";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [2];
        bcc[0] = "georgiemail03@gmail.com";
        bcc[1] = "georgiemail004@gmail.com";
        cc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
           /**
     * Test of send method with cc with html, of class EmailBean.
     */
    @Test
    public void testSendBCCHTML() {
        
        String subj = "test BCC";
        String message = "Please work";
        String attach = "";
        String html = "<a1> html test <a1>";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [2];
        bcc[0] = "georgiemail03@gmail.com";
        bcc[1] = "georgiemail004@gmail.com";
        cc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();
        
        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }
    
    /**
     * Test of send method with attachment, of class EmailBean.
     */
    @Test
    public void testSendAttachment() {
        
        String subj = "testing Attachment";
        String message = "Please work Attachments!";
        String attach = "jpg1.jpg";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [1];
        String [] bcc = new String [1];
        cc[0] = "";
        bcc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        EmailBean eb = e.send();

        if(!eb.getSubject().isEmpty())
        {
            assert(true);
        }
        else
        {
            assert(false);
        }
    }

    /**
     * Test of receive method, of class EmailBean.
     */
    @Test
    public void testReceive() {
        EmailBean e = new EmailBean();

        try
        {
            Thread.sleep(10000);
        } catch (InterruptedException ex)
        {
            System.exit(1);
        }
        
        EmailBean [] eb = e.receive("georgiemail02@gmail.com");
        
        if(eb.length != 0)
        {
            assert(true);
        }
        else
        {
            assert(false);
        }

    }

    /**
     * Test of sendWithCC method, of class EmailBean.
     */
    /*@Test
    public void testSendWithCC() {
        String subj = "test cc";
        String message = "Please work cc";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [2];
        String [] bcc = new String [2];
        cc[0] = "georgiemail02@gmail.com";
        cc[1] = "georgiemail03@gmail.com";
        bcc[0] = "";
        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        e.send();

        assertTrue(true);
    }*/

    /**
     * Test of sendWithBCC method, of class EmailBean.
     */
   /* @Test
    public void testSendWithBCC() {
        String subj = "test bcc";
        String message = "Please work bcc";
        String attach = "";
        String html = "";
        String to = "georgiemail02@gmail.com";
        String from = "georgiemail01@gmail.com";
        String [] cc = new String [2];
        String [] bcc = new String [2];
        cc[0] = "";
        bcc[0] = "georgiemail02@gmail.com";
        bcc[1] = "georgiemail03@gmail.com";

        
        EmailBean e = new EmailBean(subj, message, attach, to, from, html,
                cc, bcc);
        e.send();

        assertTrue(true);
    }*/
    
}
