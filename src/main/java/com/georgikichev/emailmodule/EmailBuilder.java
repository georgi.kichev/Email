/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.georgikichev.emailmodule;

import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author GoGo
 */
public class EmailBuilder {
    
    private String to = "";
    private String from = "";
    private String [] bcc;
    private String [] cc;
    private String attachmentPath = "";
    private String html = "";
    private String message = "";
    private String subj = "";
    private String folder = "";
    private Date received;

       
    public EmailBuilder ()
    {}
    
    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String [] getBcc() {
        return bcc;
    }

    public void setBcc(String [] bcc) {
        this.bcc = new String [bcc.length];
        for(int i = 0; i < bcc.length; i ++)
        {
            this.bcc[i] = bcc[i];
        }
    }

    public String [] getCc() { 
        return cc;
    }

    public void setCc(String [] cc) {
        this.cc = new String [cc.length];
        for(int i = 0; i < cc.length; i ++)
        {
            this.cc[i] = cc[i];
        }
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubj() {
        return subj;
    }

    public void setSubj(String subj) {
        this.subj = subj;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.from);
        hash = 53 * hash + Arrays.deepHashCode(this.bcc);
        hash = 53 * hash + Arrays.deepHashCode(this.cc);
        hash = 53 * hash + Objects.hashCode(this.attachmentPath);
        hash = 53 * hash + Objects.hashCode(this.html);
        hash = 53 * hash + Objects.hashCode(this.message);
        hash = 53 * hash + Objects.hashCode(this.subj);
        return hash;
    }
}
