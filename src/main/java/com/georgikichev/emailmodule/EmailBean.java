/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.georgikichev.emailmodule;

import java.io.File;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import jodd.mail.MailAddress;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;
import jodd.mail.ImapSslServer;
import javax.mail.Flags;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Date;
import jodd.mail.att.FileAttachment;

/**
 *
 * @author 1333539
 */
public class EmailBean extends Email {
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    
    
    private final String smtpServerName = "smtp.gmail.com";
    private final String imapServerName = "imap.gmail.com";
    private ConfigBean b = new ConfigBean();
    
    EmailBuilder e = new EmailBuilder();
    
    public EmailBean(){}
    
    public EmailBean(String subj, String text, String attach, String to,
            String from, String html, String [] cc, String [] bcc)
    {       
            //build an EmailBuilder object depending on what the email has 
            //if(subj != null && !subj.isEmpty())
            //{
                e.setSubj(subj);
                e.setMessage(text);   
                e.setTo(to);
                e.setFrom(from);
                    e.setAttachmentPath(attach);
                    e.setCc(cc);
                    e.setBcc(bcc);
                    e.setHtml(html);    
           //}
           /* else
            {
                throw new IllegalArgumentException("Cannot send without subject");
            }*/
            //TODO ReceivedDate
    }
    
    
    /**
     * Used to send an email to the receiver when both a receiver and 
     * sender are given, can have cc, bcc, html and attachments..
     * @return 
    */   
    public EmailBean send()
    {
        //create
        SmtpServer<SmtpSslServer> server = b.createSMTPServer(smtpServerName);
        
        EmailBean email = new EmailBean();
        //assgin attributes
        email.from(e.getFrom());
        email.subject(e.getSubj());
        email.to(e.getTo());
        email.addText(e.getMessage());
        if(e.getAttachmentPath() != null && !e.getAttachmentPath().isEmpty())
            email.attach(EmailAttachment.attachment().file(e.getAttachmentPath()));
        //check for html
        if(!e.getHtml().isEmpty())
            email.addHtml(e.getHtml());
        //check for CCs and bccs
        if(cc != null && !e.getCc()[0].isEmpty())
            e.setCc(e.getCc());
        else if(bcc != null && !e.getBcc()[0].isEmpty())
            e.setBcc(e.getBcc());
  
        //create session and send email
        SendMailSession session = server.createSession();
        session.open();
        session.sendMail(email);
        session.close();
        
        e.setFolder("Sent");
        
        return email;
    }
    /**
     * Method that when fed a receiver email, will import all new emails
     * that have been sent to this particular receiver. 
     * @param receiver
     * @return 
    */
    public EmailBean [] receive(String receiver)
    {
        //create IMAP server      
        ImapSslServer imapServer = b.createImap(imapServerName);
        
        //create session for server
        ReceiveMailSession session = imapServer.createSession();
        session.open();
        
        //create obj that communicates with server
        ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.
                filter().flag(Flags.Flag.SEEN, false));
        
        session.close();
        
        //convert to EmailBean
        EmailBean [] completeEmails = mergeReceives(emails);
        
        //set fodler to Inbox
        
        return completeEmails;
    }
    
    //converts ReceivedEmails to EmailBean
    private EmailBean [] mergeReceives(ReceivedEmail[] received)
    {
        //create list
        EmailBean [] emails = new EmailBean [received.length];
        
        for(int i=0; i < received.length; i++) 
        {
            emails[i].setFrom(received[i].getFrom());
            emails[i].setSubject(received[i].getSubject());
            emails[i].setTo(received[i].getTo());emails[i] = new EmailBean();
            emails[i].setBcc(received[i].getBcc());
            emails[i].setCc(received[i].getCc());
            for(EmailAttachment attachment : received[i].getAttachments()){
                    emails[i].attach(attachment);
                }
        }
        
        return emails;
    }
    
    //EQUALS METHOD NOT COMPLETE, put in EmailBuilder?
    public boolean equals(EmailBean email) {
        //get the To and From from both emails
        MailAddress []  to1 = email.getTo();
        MailAddress [] to2 = this.getTo();      
        
        //comapre lengths and if not same return false, they cannot be equal
        if(to1.length != to2.length)
            return false;
        
        String [] toList1 = new String [to1.length];
        String [] toList2 = new String [to2.length]; 
        
        //convert the To to strings
        for(int i = 0; i < toList1.length; i ++)
        {
            toList1 [i] = to1[i].toString();
            toList2 [2] = to2[2].toString();
        }
        Arrays.sort(toList1);
        Arrays.sort(toList2);
        
        //compare the 2 lists
        for(int i = 0; i < toList1.length; i++)
        {
            if(!toList1[i].equals(toList2[i]))
                return false;
        }
        //compare subject
        if(!this.getSubject().equals(email.getSubject()))
            return false;
        
        //get cc
        MailAddress [] cc1 = this.getCc();
        MailAddress [] cc2 = email.getCc();
        
        //compare length
        if(cc1.length != cc2.length)
            return false;
        
        String [] ccList1 = new String [cc1.length];
        String [] ccList2 = new String [cc2.length];
        
        //convert the cc's to strings
        for(int i = 0; i < toList1.length; i ++)
        {
            ccList1 [i] = cc1[i].toString();
            ccList2[2] = cc2[2].toString();
        }
        Arrays.sort(ccList1);
        Arrays.sort(ccList2);
        
        //compare each element
        for(int i = 0; i < cc1.length; i++)
        {
            if(!ccList1[i].equals(ccList2[i]))
                return false;
        }
        
        //get all messages for both emails
        List msg1 = this.getAllMessages();
        List msg2 = email.getAllMessages();
        
        //compare list lengths
        if(msg1.size() != msg2.size())
            return false;
                    
        EmailMessage em1;
        EmailMessage em2;
        
        String text1;
        String text2;
        
        for(int i = 0; i < msg1.size(); i++)
        {
            em1 = (EmailMessage) msg1.get(i);
            em2 = (EmailMessage) msg2.get(i);
            text1 = em1.toString();
            text2 = em2.toString();
            
            if(!text1.equals(text2))
                return false;
        }
       
       //compare attachments
       List attach1 = email.getAttachments();
       List attach2 = this.getAttachments();
        
       //compare attachment list length
       if(attach1.size() != attach2.size())
            return false;
       
       byte [] b1;
       byte [] b2;
       EmailAttachment e1;
       EmailAttachment e2;
       
       //start comparing byte arrays for attachments
       for(int i = 0; i < attach1.size(); i ++)
       {
           e1 = (EmailAttachment) attach1.get(i);
           e2 = (EmailAttachment) attach2.get(i);
           
           b1 = e1.toByteArray();
           b2 = e2.toByteArray();
           
           //if length not same, not same attachment
           if(b1.length != b2.length)
               return false;
           
           for(int j = 0; j < b1.length; j++)
           {
               //different byte, different attachment
               if(b1[i] != b2[i])
                   return false;
           }
       }
       return true;  
}
}
